using UnityEngine;
 using UnityEngine.UI;
 
 namespace FeelFreeToBorrowThisCode
 {
     [RequireComponent(typeof(HorizontalLayoutGroup))]
     public class ResponsiveColumns : MonoBehaviour
     {
         RectTransform _leftSide;
         RectTransform _rightSide;
         RectTransform _column2;
         bool _vertical = true;
 
         void Start()
         {
             RectTransform rtrans = (RectTransform)transform;
             _leftSide  = (RectTransform)rtrans.GetChild(0);
             _rightSide = (RectTransform)rtrans.GetChild(1);
             _column2 = (RectTransform)_leftSide.GetChild(1);
 
             UpdateLayout();
         }
 
         void OnRectTransformDimensionsChange()
         {
             UpdateLayout();
         }
 
         void UpdateLayout()
         {
             if (_leftSide == null) return;
 
             if (Screen.width > Screen.height)
             {
                 if (_vertical)
                 {
                     _column2.SetParent(_rightSide);
                     _rightSide.gameObject.SetActive(true);
                     _vertical = false;
                 }
             }
             else if (!_vertical)
             {
                 _column2.SetParent(_leftSide);
                 _rightSide.gameObject.SetActive(false);
                 _vertical = true;
             }
         }
     }
 }