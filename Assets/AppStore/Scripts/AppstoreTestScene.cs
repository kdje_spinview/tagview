﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using Newtonsoft.Json;


public class AppstoreTestScene : MonoBehaviour 
{
	public const string m_appID_IOS = "1455607247";
	public const string m_appID_Android = "com.orangenose.noone";

	public static void OnUpdateButtonClick()
	{
        if (!(Application.isEditor && Application.isMobilePlatform))
        {
#if UNITY_IPHONE
				AppstoreHandler.Instance.openAppInStore(m_appID_IOS);
#elif UNITY_ANDROID
				AppstoreHandler.Instance.openAppInStore(m_appID_Android);
#elif UNITY_WSA
            Application.OpenURL("ms-windows-store://pdp?productId=9nt21d67vxtr");
#endif


        }
        else
        {
            Debug.Log("AppstoreTestScene:: Cannot view app in Editor.");
        }
	}

    #region versions
#if UNITY_IPHONE
    const string CURRENT_VERSION = "1.6";
#elif UNITY_ANDROID
    const string CURRENT_VERSION = "1.0";
#elif UNITY_WSA
    const string CURRENT_VERSION = "1.7";
#endif
    #endregion


    public static bool NewVersionAvailable = false;
    class PlatformVersion
    {
        public string os;
        public string version;
    }
    private IEnumerator Start()
    {
        //TODO: Check if new version available
        UnityWebRequest www = UnityWebRequest.Get("https://api.spinviewglobal.com/api/v1/app_settings");
        www.chunkedTransfer = false;
        yield return www.SendWebRequest();

        if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.downloadHandler.text))
        {
            Debug.Log("App settings data: " + www.downloadHandler.text);
//#if UNITY_EDITOR
//            const string testresponse = "{\r\n\t\"id\": 1,\r\n\t\"hero_image_img\": {\r\n\t\t\"id\": 59697,\r\n\t\t\"directory\": \"2019_03\",\r\n\t\t\"key\": \"ONCqcviwLRXu1NzcJkYJOyLCzhvpxc\",\r\n\t\t\"original_name\": \"login_background.dea5300c.png.jpg\",\r\n\t\t\"format\": \"jpeg\",\r\n\t\t\"created_at\": \"2019-03-19T11:11:00.000Z\",\r\n\t\t\"deleted_at\": null,\r\n\t\t\"is_360\": 0,\r\n\t\t\"width\": 1920,\r\n\t\t\"height\": 1536\r\n\t},\r\n\t\"ar_app\": {\r\n\t\t\"versions\": [{\r\n\t\t\t\"os\": \"Windows\",\r\n\t\t\t\"version\": \"1.6\"\r\n\t\t}, {\r\n\t\t\t\"os\": \"iOS\",\r\n\t\t\t\"version\": \"1.6\"\r\n\t\t}, {\r\n\t\t\t\"os\": \"Android\",\r\n\t\t\t\"version\": \"0.0\"\r\n\t\t}]\r\n\t}\r\n}";
//            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(testresponse);
//#else
            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(www.downloadHandler.text);
//#endif

            if (response.ContainsKey("ar_app"))
            {
                Dictionary<string, object> arAppData = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["ar_app"].ToString());

                if (arAppData.ContainsKey("versions"))
                {
                    List<PlatformVersion> versions = JsonConvert.DeserializeObject<List<PlatformVersion>>(arAppData["versions"].ToString());

                    PlatformVersion newVersion = versions.Find(x => x.os ==
#if UNITY_IPHONE
                    "iOS"
#elif UNITY_ANDROID
                    "Android"
#elif UNITY_WSA
                    "Windows"
#else 
                    ""
#endif
                    );
                    if (newVersion == null) yield break;
                    
                    if (Util.CompareVersions(CURRENT_VERSION, newVersion.version) < 0)
                    {
                        //if new version is greater, open UpdatePopupModal
                        NewVersionAvailable = true;
                        Main.Instance.guiController.SettingsUpdateNotification.SetActive(true);
                        PopupManager.Instance.OpenPopup(PopupType.UPDATEPOPUP);
                    }
                }
            }
        }
    }
}
