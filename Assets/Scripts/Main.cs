﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Main : MonoBehaviour {
    
    public GameObject screenshotAnimation;    

    public GuiController guiController;
    public MapLocation mapLocation;
    public GameObject buttons;
    public Transform NotificationsRoot;
    public GameObject NotificationPrefab;
    public LoadingBar loading;
    public RawImage CompanyRawImage;
    public RectTransform canvasRectTransform;
    public CompanyLoading _companyLoading;
    public LoadingBlur loadingBlur;

    public Transform PopupRoot;

    public GameObject arRoot;
    public GameObject welcome;

    public Button LetsGoButton;
    

    private void Awake()
    {
        Settings.arSSFolderID = "";

        const float defaultValue = 5f;
        UnityEngine.EventSystems.EventSystem.current.pixelDragThreshold = (int)Mathf.Max(defaultValue, (int)(defaultValue * Screen.dpi / 200f));
#if UNITY_WSA
        arVuforia.SetActive(true);
#else
        welcome.SetActive(true);
        LetsGoButton.onClick.AddListener(() => { StartAR(); });
#endif
        //PlayerPrefs.DeleteAll();
#if UNITY_EDITOR
        LocalizationController.Instance.CurrentLanguage = Prefs.Language;
#endif
    }
    
    // Use this for initialization
    void Start()
    {
        //bool connection = false;
        StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            //connection = isConnected;
            if(!isConnected)
                Notify(Settings.NO_INTERNET);
        }));

        //if (!connection)
        //{
        //    Notify(Settings.NO_INTERNET);
        //}
        //else
        {
            InitMap();
            
            //jsonReader.LoadData();
            //CreatePOSListDemo();

            _companyLoading.StartLoading();
        }

        //portrait orientation//_currentOrientation = Orientation.NONE;
        //portrait orientation//StartCoroutine(UpdateOrientation());
        
#if !UNITY_WSA && !UNITY_ANDROID
        StartAR();
#endif
    }

#if !UNITY_WSA
    public void StartAR()
    {
#if !UNITY_WSA
        GameObject ar = Instantiate(arRoot);
        InteractionController.Instance.SetupAR(ar.transform.GetChild(0).gameObject);
#endif
        welcome.SetActive(false);
    }
#endif

    void InitMap()
    {
    //Bug: first map is not saved properly. 
    //Fix: force map image creation at the scene start
        mapLocation.CreateMapImage(/*Map"*/false);
    }

    //for deleting cached asset bundles
    public static void CleanCache()
    {
        if (Caching.ClearCache())
        {
            Debug.Log("Successfully cleaned the cache.");
        }
        else
        {
            Debug.Log("Cache is being used.");
        }
    }
    

    public IEnumerator CheckInternetConnection(Action<bool> action)
    {
        UnityWebRequest www = new UnityWebRequest("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    public void Notify(string messageKey)
    {
        GameObject notification = Instantiate(NotificationPrefab, NotificationsRoot);
        LocalizationElement lE = notification.GetComponentInChildren<LocalizationElement>();
        lE.Key = messageKey;
        lE.SetText();
        Destroy(notification, 4f);
    }

    public IEnumerator RefreshAfterOneFrame(GameObject go)
    {
        go.SetActive(false);

        //yield return new WaitForSeconds(0.1f);
        yield return new WaitForEndOfFrame();

        go.SetActive(true);
    }
#region BUTTONS
    public void OpenSettings()
    {
        buttons.SetActive(false);
        PopupManager.Instance.OpenPopup(PopupType.SETTINGS);
    }
    public void ScreenShotButton()
    {
        PopupManager.Instance.ClosePopupIfLast(PopupType.INFO);
        PopupManager.Instance.OpenPopup(PopupType.SCREENSHOT);
    }
#endregion

#region SINGLETON
    private static Main instance = null;

    public static Main Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<Main>();
            }

            return instance;
        }
    }
#endregion

#if UNITY_EDITOR
    [MenuItem("AR/Clear Player Prefs")]
    static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PlayerPrefs cleared");
    }
#endif

    public void ReloadProductPopup()
    {
        PopupManager.Instance.ClosePopup();
        PopupManager.Instance.OpenPopup(PopupType.PRODUCT);
    }
    
}
