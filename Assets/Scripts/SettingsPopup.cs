﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Lean.Touch;


public class SettingsPopup : Popup
{
    public Transform LangRoot;
    public GameObject LangPrefab;
    public Text DisplayName;
    public Text Email;
    public Image UserImage;
    public Button SignOutButton;
    public Button QuitButton;    
    public GameObject UpdateRoot;
    public Button UpdateButton;

    public GameObject BusinessCardRoot;
    public Button BusinessCardButton;

    List<LanguageItem> _languageItems;


    private void Awake()
    {
        SignOutButton.onClick.AddListener(SignOut);
        QuitButton.onClick.AddListener(Quit);
        UpdateButton.onClick.AddListener(AppstoreTestScene.OnUpdateButtonClick);

        bool showBusinessRoot = ShowBusinessRoot();
        BusinessCardRoot.SetActive(showBusinessRoot);
        BusinessCardButton.onClick.AddListener(() => SceneManager.LoadScene("BusinessCard"));
    }
    HashSet<string> businessCardsMails = new HashSet<string>() { "tori@spinview.io", "arview@spinview.io", "spinview@spinview.io" };
    bool ShowBusinessRoot()
    {
        return false && businessCardsMails.Contains(Settings.email);
    }
    private void Start()
    {
        DisplayName.text = Settings.displayName;
        Email.text = Settings.email;

        LanguageItem._settings = this;
        var langs = Util.GetEnumList<Language>();
        _languageItems = new List<LanguageItem>();
        foreach (var lang in langs)
        {
            LanguageItem portraitLang = Instantiate(LangPrefab, LangRoot).GetComponent<LanguageItem>();
            portraitLang.Language = lang;
            _languageItems.Add(portraitLang);
        }
        Check(Prefs.Language);

        StartCoroutine(GetUserImage());
    }
    public void Check(Language language)
    {
        if (_languageItems != null)
            foreach (var lang in _languageItems)
                lang.Check(language);
    }
    static Dictionary<string, Sprite> _spritesDict = new Dictionary<string, Sprite>();
    IEnumerator GetUserImage()
    {
        string imageUrl = "https://api.spinviewglobal.com/api/v1/images/" + Settings.imageKey+ "?size=100x100";
        Sprite imageSprite = null;
        if (_spritesDict.ContainsKey(imageUrl)) imageSprite = _spritesDict[imageUrl];
        else {
            //WWW www = new WWW(imageUrl);
            int token = WebRequest.GetNewToken();
            yield return WebRequest.Get(token, imageUrl, true);

            //yield return www;
            Texture2D texture2d = WebRequest.TextureData(token);
            //if (www.texture != null)
            if(!WebRequest.Error(token) && texture2d != null)
                imageSprite = Sprite.Create(texture2d, new Rect(0, 0, texture2d.width, texture2d.height), new Vector2(0, 0));
        }
        UserImage.sprite = imageSprite;
    }
    public void SignOut()
    {
        Prefs.ResetLoginData();
        SceneManager.LoadScene(0);

    }
    public void Quit()
    {
        //Application.Quit();
        SceneManager.LoadScene("LoadScene");
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {        
        if (AppstoreTestScene.NewVersionAvailable)
        {
            UpdateRoot.SetActive(true);
            Main.Instance.guiController.SettingsUpdateNotification.SetActive(false);
        }
    }

    public override void ClosePopup()
    {
        Main.Instance.buttons.SetActive(true);
    }
}
