﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationHandler : MonoBehaviour
{
    public static OrientationHandler Instance;
    Orientation _currentOrientation = Orientation.NONE;
    WaitForSeconds waiter;

    private void Awake()
    {
        Instance = this;
        _orientationChangedActions = new List<Action<Orientation>>();
        waiter = new WaitForSeconds(0.25f);
    }

    private void Start()
    {
        StartCoroutine(CheckOrientation());
    }

    IEnumerator CheckOrientation()
    {
        while (true)
        {
            Orientation newOrientation = Util.GetOrientation();
            if(_currentOrientation != newOrientation)
            {
                TriggerOrientationChanged(newOrientation);
                _currentOrientation = newOrientation;
            }

            yield return waiter;
        }
    }

    void TriggerOrientationChanged(Orientation newOrientation)
    {
        foreach (Action<Orientation> orientationAction in _orientationChangedActions)
            orientationAction(newOrientation);
    }


    List<Action<Orientation>> _orientationChangedActions;
    public void SubscribeOrientationChanged(Action<Orientation> action)
    {
        _orientationChangedActions.Add(action);
    }
    public void UnsubscribeOrientationChanged(Action<Orientation> action)
    {
        _orientationChangedActions.Remove(action);
    }
}
