﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LogIn : MonoBehaviour {

    public Button QuitButton;

    public InputField Username;
    public InputField Password;
    public InputField ForgottenPassword;    

    bool _currentContentType = false;
    bool PasswordContentType//true is Standard, false is Password
    {
        get
        {
            return _currentContentType;
        }
        set
        {
            _currentContentType = value;

            Password.contentType = _currentContentType? InputField.ContentType.Standard : InputField.ContentType.Password;
            ShowPassword.SetActive(_currentContentType);
            HidePassword.SetActive(!_currentContentType);

            Password.ForceLabelUpdate();

        }
    }

    public Text Error;
    public string ErrorText
    {
        set
        {
            Error.text = value;
        }
    }

    public Button LogInButton;
    bool LogInButtonInteractable
    {
        set
        {
            LogInButton.interactable = value;
        }
    }
    public GameObject ShowPassword;
    public GameObject HidePassword;

    string api = "https://api.spinviewglobal.com/api/v1/sessions/login";

    public Button ForgotPasswordButton;

    public LoadingBlur loadingBlur;

    public static LogIn instance;
    Color hexGrey;
    private void Awake()
    {
        instance = this;

        QuitButton.onClick.AddListener(ApplicationQuit);
        ForgotPasswordButton.onClick.AddListener(ForgotPassword);

        //PlayerPrefs.DeleteAll();

        LocalizationController.Instance.CurrentLanguage = Prefs.Language;
        Dictionary<string, string> logInData = Prefs.GetLogInData();
        Debug.Log("LogIn  (logInData != null) " + (logInData != null));
        if (logInData != null)
        {
            StartCoroutine(AutoLogin());
        }

        hexGrey = new Color();
        ColorUtility.TryParseHtmlString("#f4f4f4", out hexGrey);
    }
    Color greyColorForIF;
    private void Start()
    {        
        Password.asteriskChar = '•';
        _currentOrientation = Orientation.NONE;

        //SetLoginSceneVersion(Util.GetDevice());

        StartCoroutine(FixOrientation());

        StartCoroutine(SetInputCarets());

        if (Util.IsIPhoneX())
        {
            QuitButton.gameObject.transform.localScale = Vector3.one * 1.2f;
        }
    }
    DeviceType _deviceType;

    IEnumerator SetInputCarets()
    {
        yield return new WaitForEndOfFrame();
        Transform caretGO1 = Username.transform.Find(Username.transform.name + " Input Caret");
        if (caretGO1 != null) caretGO1.GetComponent<CanvasRenderer>().SetMaterial(Graphic.defaultGraphicMaterial, Texture2D.whiteTexture);
        Transform caretGO3 = Password.transform.Find(Password.transform.name + " Input Caret");
        if (caretGO3 != null) caretGO3.GetComponent<CanvasRenderer>().SetMaterial(Graphic.defaultGraphicMaterial, Texture2D.whiteTexture);
    }
    #region device_and_orientation
    public Transform Root;
    public GameObject ForgotPasswordRoot;
    public GameObject CheckYourEmailRoot;

    Orientation _currentOrientation = Orientation.NONE;
    IEnumerator FixOrientation()
    {
        _deviceType = Util.GetDevice();
        while (true)
        {
            Orientation newOrientation = Util.GetOrientation();
            if(_currentOrientation != newOrientation)
            {
                SetOrientationRoots(newOrientation);
                _currentOrientation = newOrientation;
            }
            yield return new WaitForSeconds(0.25f);
        }
    }
    void SetOrientationRoots(Orientation orientation)
    {
        float screenWidth = Util.ScreenWidth;
        float screenHeight = Util.ScreenHeight;
        float smallerDim = screenWidth < screenHeight ? screenWidth : screenHeight;
        float biggerDim = screenWidth > screenHeight ? screenWidth : screenHeight;

        switch (orientation)
        {
            case Orientation.PORTRAIT:
                switch (_deviceType)
                {
                    case DeviceType.MOBILE:
                        Root.localScale = Vector3.one * smallerDim / 1080f;//1080 - referenced portrait width
                        ForgotPasswordRoot.transform.localScale = Vector3.one * smallerDim / 1080f;
                        break;
                    case DeviceType.TABLET:
                        Root.localScale = Vector3.one * (smallerDim / 1080f / 3);
                        ForgotPasswordRoot.transform.localScale = Vector3.one * (smallerDim / 1080f / 3);
                        break;
                }
                Root.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;//centered
                break;
            case Orientation.LANDSCAPE:
                switch (_deviceType)
                {
                    case DeviceType.MOBILE:
                        Root.localScale = Vector3.one * (1080f / 1400f);
                        Root.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -smallerDim / 10f);
                        ForgotPasswordRoot.transform.localScale = Vector3.one * (1080f / 1400f);
                        ForgotPasswordRoot.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -smallerDim / 10f);
                        break;
                    case DeviceType.TABLET:
                        Root.localScale = Vector3.one * (smallerDim / 1080f / 3);
                        Root.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -smallerDim / 30f);
                        ForgotPasswordRoot.transform.localScale = Vector3.one * (smallerDim / 1080f / 3);
                        ForgotPasswordRoot.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -smallerDim / 30f);
                        break;
                }
                break;
        }
    }
    #endregion
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && LogInButton.interactable)
        {
            LogInButtonClicked();
        }
        if(Input.GetKey(KeyCode.Tab))
        {
            if (_currentlySelectedType == SelectableType.USERNAME)
                Password.Select();
        }
    }

    #region onvaluechange
    //both username and password are calling this
    public void OnValueChanged()
    {
        ErrorText = "";
        LogInButtonInteractable = !(Username.text.Equals("") || Password.text.Equals(""));
    }
    #endregion

    public void LogInButtonClicked()
    {
        ErrorText = "";
        LogInButtonInteractable = false;

        SelectedGraphicsUsername.SetActive(false);
        SelectedGraphicsPassword.SetActive(false);

        //SetNormalColor(Username, Color.white);
        //SetNormalColor(Password, hexGrey);
        SetInputFieldSprite(Username, false);
        SetInputFieldSprite(Password, true);

        StartCoroutine(LogInCoroutine(Username.text, Password.text));
    }

    public void PasswordToggleAction()
    {
        PasswordContentType = !PasswordContentType;
    }
    public void ApplicationQuit()
    {
        Debug.Log("ApplicationQuit called");
        //Application.Quit();
        SceneManager.LoadScene("LoadScene");
    }
    void ForgotPassword()
    {
        ActivateLogInRoot(LogInRoot.FORGOTPASSWORD);
    }
    public enum LogInRoot
    {
        LOGIN,
        FORGOTPASSWORD,
        CHECKYOUREMAIL
    }
    public void ActivateLogInRoot(LogInRoot type)
    {
        SetSelectGraphics(SelectableType.NONE);
        Root.gameObject.SetActive(type == LogInRoot.LOGIN);
        ForgotPasswordRoot.SetActive(type == LogInRoot.FORGOTPASSWORD);
        CheckYourEmailRoot.SetActive(type == LogInRoot.CHECKYOUREMAIL);
    }
    public IEnumerator CheckInternetConnection(Action<bool> action)
    {
        UnityWebRequest www = new UnityWebRequest("http://google.com");
        yield return www;
        if (www.error != null)
        {
            Debug.Log("JOKAN internet error = " + www.error);
            action(false);
        }
        else
        {
            Debug.Log("JOKAN internet ok");
            action(true);
        }
    }

    IEnumerator AutoLogin()
    {
        SetSettingsFields();

        bool connection = false;
        yield return StartCoroutine(CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (connection)
        {
            loadingBlur.Activate();

            bool token = false;
            yield return StartCoroutine(CheckToken((isValid) => {
                // handle connection status here
                token = isValid;
            }));

            if (token)
            {
                //token ispravan, koristi se on i radi se dalje login
                StartCoroutine(StartLogInProcess());
            }
            else
            {
                //token los, radi se novi login
                string username = Prefs.GetUsername();
                string password = Prefs.GetPassword();

                string jsonString = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "username", username }, { "password", password } });
                Dictionary<string, string> header = new Dictionary<string, string>();
                header.Add("Content-Type", "application/json");
                byte[] body = Encoding.UTF8.GetBytes(jsonString);
                WWW wwww = new WWW(api, body, header);
               
                yield return new WaitForEndOfFrame();

                yield return wwww;
                LogInButtonInteractable = true;
                if (wwww.error == null)
                {
                    try
                    {
                        Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(wwww.text);
                        if (response.ContainsKey("session"))
                        {
                            Dictionary<string, object> response1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["session"].ToString());
                            if (response1.ContainsKey("access_token") && response1["access_token"] != null && response1["access_token"].ToString() != "")
                            {
                                Settings.accessToken = response1["access_token"].ToString();
                            }
                        }

                        StartCoroutine(StartLogInProcess());
                    }
                    catch (System.Exception)
                    {
                    }

                }
                else
                {
                    loadingBlur.Deactivate();
                    //ErrorText = LocalizationController.Instance.GetTranslation("invalid_username_or_password");// "Invalid username or password.";
                }
            }
        }
        else
        {
            //offline mode
            if (!Settings.accessToken.Equals(""))
            {
                ErrorText = LocalizationController.Instance.GetTranslation(Settings.NO_INTERNET);
                Settings.PlatformModels = Settings.StoredModels;
                SceneManager.LoadScene(Settings.arScene);
            }
        }
    }

    void SetSettingsFields()
    {
        string companyName = Prefs.GetCompanyName();
        string displayName = Prefs.GetDisplayName();
        string eMail = Prefs.GetEMail();
        string imageKey = Prefs.GetImageKey();
        string companyImageKey = Prefs.GetCompanyImageKey();
        string folderID = Prefs.GetFolderID();
        string companyID = Prefs.GetCompanyID();
        string accessToken = Prefs.GetAccessToken();

        Settings.companyName = companyName;
        Settings.displayName = displayName;
        Settings.email = eMail;
        Settings.imageKey = imageKey;
        Settings.companyImageKey = companyImageKey;
        Settings.rootFolderID = folderID;
        Settings.companyID = companyID;
        Settings.accessToken = accessToken;
        Settings.StoredModels = Util.ParseDictionary(Prefs.GetJsonDataPref());
    }

    IEnumerator StartLogInProcess()
    {
        yield return StartCoroutine(FTPManager.Instance.GetPlatformData());
        Debug.Log("Load Scene");
        SceneManager.LoadScene(Settings.arScene);
    }

    IEnumerator LogInCoroutine(string username, string password)
    {
        bool connection = false;
        yield return StartCoroutine(CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            ErrorText = LocalizationController.Instance.GetTranslation(Settings.NO_INTERNET);
            yield break;
        }
        string jsonString = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "username", username }, { "password", password} });
        Dictionary<string, string> header = new Dictionary<string, string>();
        header.Add("Content-Type", "application/json");
        byte[] body = Encoding.UTF8.GetBytes(jsonString);
        WWW wwww = new WWW(api, body, header);

        yield return new WaitForEndOfFrame();

        yield return wwww;
        LogInButtonInteractable = true;
        if (wwww.error == null)
        {
            try
            {
                //reset user data
                Settings.companyName = Settings.displayName = Settings.email = Settings.imageKey = Settings.companyImageKey = string.Empty;

                Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(wwww.text);
                if (response.ContainsKey("session"))
                {
                    Dictionary<string, object> response1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["session"].ToString());
                    if (response1.ContainsKey("access_token") && response1["access_token"] != null && response1["access_token"].ToString() != "")
                    {
                        Settings.accessToken = response1["access_token"].ToString();
                    }
                    if (response1.ContainsKey("company_id") && response1["company_id"] != null && response1["company_id"].ToString() != "")
                    {
                        Settings.companyID = response1["company_id"].ToString();
                    }
                }
                if (response.ContainsKey("cwd"))
                {
                    Dictionary<string, object> response2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["cwd"].ToString());
                    if (response2.ContainsKey("company") && response2["company"] != null)
                    {
                        Dictionary<string, object> response3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["company"].ToString());
                        if (response3.ContainsKey("name") && response3["name"] != null)
                            Settings.companyName = response3["name"].ToString();       
                    }
                    if (response2.ContainsKey("folder") && response2["folder"] != null)
                    {
                        Dictionary<string, object> response23 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response2["folder"].ToString());
                        if (response23.ContainsKey("id") && response23["id"] != null)
                        {
                            Settings.rootFolderID = response23["id"].ToString();
                        }
                    }
                }
                if (response.ContainsKey("uwd") && response["uwd"] != null)
                {
                    Dictionary<string, object> response22 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response["uwd"].ToString());
                    if (response22.ContainsKey("user") && response22["user"] != null)
                    {
                        Dictionary<string, object> response32 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response22["user"].ToString());
                        if (response32.ContainsKey("display_name") && response32["display_name"] != null)
                            Settings.displayName = response32["display_name"].ToString();
                        if (response32.ContainsKey("email") && response32["email"] != null)
                            Settings.email = response32["email"].ToString();

                    }
                    if (response22.ContainsKey("avatar") && response22["avatar"] != null)
                    {
                        Dictionary<string, object> response33 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response22["avatar"].ToString());
                        if (response33.ContainsKey("key"))
                            Settings.imageKey = response33["key"].ToString();
                    }
                    if (response22.ContainsKey("cwds") && response22["cwds"] != null)
                    {
                        List<object> response34 = JsonConvert.DeserializeObject<List<object>>(response22["cwds"].ToString());
                        if (response34 != null && response34.Count > 0)
                        {
                            Dictionary<string, object> response345 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response34[0].ToString());
                            if (response345.ContainsKey("logo") && response345["logo"] != null)
                            {
                                Dictionary<string, object> response45 = JsonConvert.DeserializeObject<Dictionary<string, object>>(response345["logo"].ToString());
                                if (response45.ContainsKey("key") && response45["key"] != null)
                                   Settings.companyImageKey = response45["key"].ToString();
                            }
                        }
                    }
                }
                Prefs.SetLoginData(username, password, Settings.companyName, Settings.displayName, Settings.email, Settings.imageKey, Settings.companyImageKey, Settings.rootFolderID, Settings.companyID, Settings.accessToken);
            }
            catch (System.Exception)
            {
                ErrorText = LocalizationController.Instance.GetTranslation(Settings.SOMETHING_WENT_WRONG);// "Something went wrong. Please try again.";
            }

            //if the load failed for some reason, the name will be default (SpinView)
            Settings.MAIN_URL = Settings.PLATFORM_URL +  Settings.accessToken;
            loadingBlur.Deactivate();

            StartCoroutine(StartLogInProcess());
        }
        else
        {
            ErrorText = LocalizationController.Instance.GetTranslation(Settings.INVALID_LOGIN);// "Invalid username or password.";
        }      
    }

    IEnumerator CheckToken(Action<bool> action)
    {
        if (Settings.accessToken.Equals(""))
        {
            action(false);
        }

        string link = "https://api.spinviewglobal.com/api/v1/sessions/by-token/" + Settings.accessToken;

        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, link);
        if (!WebRequest.Error(token))
        {
            var www = WebRequest.WWW(token);

            if (www == null)
            {
                action(false);
            }

            Dictionary<string, object> response = JsonConvert.DeserializeObject<Dictionary<string, object>>(www.downloadHandler.text);
            if (response.ContainsKey("error"))
            {
                action(false);
            }
            else
            {
                action(true);
            }
        }
        else
        {
            Debug.Log("JOKAN internet ok");
            action(false);
        }
    }

    public void TempLogin()
    {
        SceneManager.LoadScene("LoadScene");
    }
    #region selected_graphics
    public GameObject SelectedGraphicsUsername;
    public GameObject SelectedGraphicsPassword;
    public GameObject SelectedGraphicsForgottenPassword;

    SelectableType _currentlySelectedType;
    public void SetSelectGraphics(SelectableType type)
    {
        _currentlySelectedType = type;

        SelectedGraphicsUsername.SetActive(false);
        SelectedGraphicsPassword.SetActive(false);
        SelectedGraphicsForgottenPassword.SetActive(false);

        //SetNormalColor(Username, hexGrey);
        //SetNormalColor(Password, hexGrey);
        //SetNormalColor(ForgottenPassword, hexGrey);
        SetInputFieldSprite(Username, true);
        SetInputFieldSprite(Password, true);
        SetInputFieldSprite(ForgottenPassword, true);
        switch (type)
        {
            case SelectableType.USERNAME:
                SelectedGraphicsUsername.SetActive(true);
                //SetNormalColor(Username, Color.white);
                SetInputFieldSprite(Username, false);
                break;
            case SelectableType.PASSWORD:
                SelectedGraphicsPassword.SetActive(true);
                //SetNormalColor(Password, Color.white);
                SetInputFieldSprite(Password, false);
                break;
            case SelectableType.FORGOTTENPASSWORD:
                SelectedGraphicsForgottenPassword.SetActive(true);
                //SetNormalColor(ForgottenPassword, Color.white);
                SetInputFieldSprite(ForgottenPassword, false);
                break;
        }
    }

    void SetNormalColor(InputField iF, Color newColor)
    {
        ColorBlock cb = iF.colors;
        cb.normalColor = newColor;
        iF.colors = cb;
    }
    public Sprite InputFieldGreySprite;
    void SetInputFieldSprite(InputField iF, bool sprite)
    {
        iF.image.sprite = sprite ? InputFieldGreySprite : null;
    }
#endregion

#region notifications
    public Transform NotificationsRoot;
    public GameObject NotificationPrefab;
    public void Notify(string messageKey)
    {
        GameObject notification = Instantiate(NotificationPrefab, NotificationsRoot);
        LocalizationElement lE = notification.GetComponentInChildren<LocalizationElement>();
        lE.Key = messageKey;
        lE.SetText();
        Destroy(notification, 4f);
    }
#endregion
}
