﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class MailPopup : Popup
{
    public GameObject MailItemPrefab;

    public EmailsRoot ToRoot;
    public EmailsRoot CcRoot;
    public Transform ToRootTransform;
    public Transform CcRootTransform;

    public MailSelected _mailselected;

    ScreenshotManager _screenshotPopup;

    #region popup
    string imageTitle;
    public override void OpenPopup(Dictionary<string, object> data)
    {

        _screenshotPopup = (ScreenshotManager)PopupManager.Instance.GetPopup(PopupType.SCREENSHOT);
        if (_screenshotPopup == null)
        {
            PopupManager.Instance.ClosePopup();
        }
        else
        {
            if (data.ContainsKey("filename"))
            {
                imageTitle = (string)(data["filename"]);
            }

            //if (data.ContainsKey("ScreenshotImageMobile"))
            {
                ScreenshotImage.sprite = _screenshotPopup.ScreenshotImage.sprite;
                Main.Instance.StartCoroutine(GetMapImage());
            }

            Util.SetImageText(ImageNameText, imageTitle, 15);
            Util.SetImageText(MapImageNameText, "Map" + imageTitle, 17);
            //ImageNameText.text = _screenshotPopup.emailScreenshotFilename;
            //MapImageNameText.text = _screenshotPopup.emailMapScreenshotFilename;
        }
        ScreenshotTicked = true;
        MapTicked = true;

        ScreenshotImageButton.onClick.AddListener(() => ScreenshotTicked = !ScreenshotTicked);
        MapImageButton.onClick.AddListener(() => MapTicked = !MapTicked);

        if (Util.IsIPhoneX())
        {
            ScreenshotImage.GetComponent<RectTransform>().sizeDelta = new Vector2(400f, 388f);
            MapImage.GetComponent<RectTransform>().sizeDelta = new Vector2(400f, 388f);
        }
    }
    IEnumerator GetMapImage()
    {
        while (true)
        {
            if (Main.Instance.mapLocation.mapBytes != null) break;
            Debug.Log("MapImage: not downloaded");
            yield return new WaitForSeconds(1f);
        }
        Debug.Log("MapImage: GetMapImage start");
        Texture2D texture = new Texture2D(111, 114);
        texture.LoadImage(Main.Instance.mapLocation.mapBytes);
        Sprite mapSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(.5f, .5f), 100, 0, SpriteMeshType.FullRect);
        MapImage.sprite = mapSprite;
        Debug.Log("MapImage: GetMapImage end");
    }

    public override void ClosePopup()
    {
        ScreenshotImage.sprite = null;
        MapImage.sprite = null;
    }
    #endregion


    private void Awake()
    {
        BodyInputField.onValueChanged.AddListener(OnBodyInputFieldChange);
        SubjectInputField.onValueChanged.AddListener(OnSubjectInputFieldChange);
        SendButton.onClick.AddListener(Send);
        VoidButton.onClick.AddListener(() => { BodyInputField.Select(); });

        if (LocalizationController.Instance.CurrentLanguage == Language.GERMAN)
            SubjectInputField.GetComponent<RectTransform>().offsetMin = new Vector2(60f, SubjectInputField.GetComponent<RectTransform>().offsetMin.y);
    }

    #region top
    public Button SendButton;
    public Text SendButtonText;

    public void Send()
    {
        Debug.Log("Mailing: Send");
        string errorMessage = CheckMail();
        if (string.IsNullOrEmpty(errorMessage))
            StartCoroutine(SendMessage());
        else
            Main.Instance.Notify(errorMessage);
    }
    void SetSendButtonColor(bool enable)
    {
        Color myColor = new Color();
        ColorUtility.TryParseHtmlString(enable ? "#00a49a" : "#8b8b8b", out myColor);
        SendButtonText.color = myColor;
    }
    public string CheckMail()
    {
        SetSendButtonColor(false);
        if (ToRoot.Mails.Count == 0) return Settings.EMAIL_NOT_VALID;
        if (string.IsNullOrEmpty(SubjectInputField.text)) return Settings.IMAGE_SAVED_DEVICE;
        SetSendButtonColor(true);
        return null;
    }

    IEnumerator SendMessage()
    {
        bool connection = false;
        yield return StartCoroutine(Main.Instance.CheckInternetConnection((isConnected) => {
            // handle connection status here
            connection = isConnected;
        }));

        if (!connection)
        {
            Main.Instance.Notify(Settings.NO_INTERNET);
            yield break;
        }

        SendButton.interactable = false;
        SendButton.GetComponentInChildren<Text>().color = Color.gray;
        Main.Instance.loadingBlur.Activate("sending");

        yield return new WaitForSeconds(1f);

        UnityWebRequest www = null;
        try
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            string emailList = string.Join(";", ToRoot.Mails);
            string ccList = string.Join(";", CcRoot.Mails);
            string subject = SubjectInputField.text;
            string bodytext = BodyInputField.text;

            formData.Add(new MultipartFormDataSection("to", emailList));
            if (false && CcRoot.Mails.Count != 0) formData.Add(new MultipartFormDataSection("cc", ccList));
            if (!string.IsNullOrEmpty(bodytext))
                formData.Add(new MultipartFormDataSection("message", bodytext));
            formData.Add(new MultipartFormDataSection("subject", subject));

            if (_screenshotPopup.imageBytes != null && ScreenshotTicked) formData.Add(new MultipartFormFileSection(imageTitle/*"attachment"*/, _screenshotPopup.imageBytes, imageTitle, "image"));
            if (Main.Instance.mapLocation.mapBytes != null && MapTicked) formData.Add(new MultipartFormFileSection("Map" + imageTitle/*"attachment1"*/, Main.Instance.mapLocation.mapBytes, "Map" + imageTitle, "image"));

            www = UnityWebRequest.Post("api.spinviewglobal.com/api/v1/notifications/send/ar-app-notification", formData);

        }
        catch (Exception e)
        {
            Debug.Log("MailingSystem>SendMessage error = " + e.ToString());
            Close(www);
            yield break;
        }

        //Main.Instance.loading.StartLoading(www);
        yield return www.SendWebRequest();

        Close(www);
    }
    void Close(UnityWebRequest www)
    {
        Main.Instance.loadingBlur.Deactivate();
        if (www == null || www.isNetworkError || www.isHttpError)
        {
            Main.Instance.Notify(Settings.SOMETHING_WENT_WRONG);
            SendButton.interactable = true;
        }
        else
        {
            Main.Instance.Notify(Settings.EMAIL_SENT);
            PopupManager.Instance.ClosePopup();
        }
    }
    #endregion

    #region subject
    public InputField SubjectInputField;
    void OnSubjectInputFieldChange(string value)
    {
        CheckMail();

        //TextGenerator tg = SubjectInputField.textComponent.cachedTextGenerator;
        //int kText = tg.characterCount;
        //for (int i = 0; i < kText; ++i)
        //{
        //    if (SubjectInputField.text[i] == 'x')
        //    {
        //        foreach(var vert in tg.verts)
        //        {
        //            Debug.Log("vert: [" + vert.position.x + ", " + vert.position.y + "]");
        //        }
        //        //Debug.Log("found x at [" + tg.verts[i * 4].position.x + "," + tg.verts[i * 4].position.y + "]");
        //        //Debug.Log("found x at [" + tg.verts[(i + 1) * 4 - 2].position.x + "," + tg.verts[(i + 1) * 4 - 2].position.y + "]");
        //    }
        //}
    }
    void OnSubjectInputFieldEnd(string value)
    {
        BodyInputField.Select();
    }
    #endregion

    #region body
    public InputField BodyInputField;
    public RectTransform BodyInputFieldRectTransform;
    public Text BodyShownText;
    public RectTransform BodyParentTextRectTransform;

    public LayoutElement BodyInputRootLayout;

    string oldValue = string.Empty;
    void OnBodyInputFieldChange(string value)
    {
        int diff = Util.GetFirstDiffChar(oldValue, value);
        
        BodyShownText.text = value;
        float bodyHeight = BodyParentTextRectTransform.sizeDelta.y + 100f * 2;
        //BodyInputFieldRectTransform.sizeDelta = new Vector2(BodyInputFieldRectTransform.sizeDelta.x, bodyHeight);
        ChangeVoidHeight(BodyInputRootLayout.preferredHeight, bodyHeight);
        BodyInputRootLayout.preferredHeight = bodyHeight;
        
        oldValue = value;

        StartCoroutine(CheckBodyWidth(diff));
    }
    bool _checking = false;
    IEnumerator CheckBodyWidth(int diff)
    {
        while (_checking) yield return null;
        _checking = true;
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        if (BodyParentTextRectTransform.rect.width > 980f)
        {
            BodyInputField.text = BodyInputField.text.Insert(diff, "\n");

            BodyInputField.caretPosition++;
        }
    }
    #endregion

    #region images
    public Image ScreenshotImage;
    public Image MapImage;
    public Button ScreenshotImageButton;
    public Button MapImageButton;
    public Image ScreenshotImageTicked;
    public Image MapImageTicked;
    public Sprite ImageTicked;
    public Sprite ImageUnTicked;

    public Text ImageNameText;
    public Text MapImageNameText;

    bool _screenshotTicked;
    bool ScreenshotTicked
    {
        get
        {
            return _screenshotTicked;
        }
        set
        {
            _screenshotTicked = value;
            ScreenshotImageTicked.sprite = _screenshotTicked ? ImageTicked : ImageUnTicked;
        }
    }
    bool _mapTicked;
    bool MapTicked
    {
        get
        {
            return _mapTicked;
        }
        set
        {
            _mapTicked = value;
            MapImageTicked.sprite = _mapTicked ? ImageTicked : ImageUnTicked;
        }
    }


    public LayoutElement _void;
    float VoidHeight
    {
        get
        {
            return _void.preferredHeight;
        }
        set
        {
            _void.preferredHeight = value;
        }
    }
    public Button VoidButton;
    public void ChangeVoidHeight(float oldHeight, float newHeight)
    {
        float diff = newHeight - oldHeight;

        if (diff == 0) return;
        else if (diff < 0)
            VoidHeight -= diff;
        else// (diff > 0)
        {
            if (VoidHeight < diff)
                VoidHeight = 0;
            else
                VoidHeight -= diff;
        }
    }
    #endregion


    public void SelectMail(MailItem mailItem, EmailsRoot Root)
    {
        _mailselected.gameObject.SetActive(true);
        _mailselected.SetMailsPosition(mailItem, Root);
    }
}
