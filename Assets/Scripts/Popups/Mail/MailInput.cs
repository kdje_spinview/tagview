﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MailInput : MonoBehaviour, ISelectHandler//, IDeselectHandler
{
    public RectTransform Rect;
    public EmailsRoot MyRoot;
    public InputField Input;

    public Image MyInputCaret;

    private string _mailText;

    bool _typing = false;
    bool typing
    {
        get
        {
            return _typing;
        }
        set
        {
            MyInputCaret.gameObject.SetActive(value);
               _typing = value;
        }
    }
    public void OnSelect(BaseEventData eventData)
    {
        Debug.Log("OnSelect");
        if (typing) return;
        Debug.Log(this.gameObject.name + " was selected");
        typing = true;
        StartCoroutine(AddMailNotCreated());
    }

    //public void OnDeselect(BaseEventData eventData)
    //{
    //    OnInputEnd();
    //}

    IEnumerator AddMailNotCreated()
    {
        MailPopup mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
        yield return MyRoot.AddEmail(mailPopup.MailItemPrefab, false);
        Input.text = string.Empty;
    }
    void Update()
    {
        if (MyRoot._lastMailText == null) return;
        //Debug.Log("MailInput>Update (MyRoot._lastMailText != null)");
        Transform InputFieldCaret = transform.GetChild(0);
        //Debug.Log("Transform InputFieldCaret = " + transform.childCount + " " + transform.GetChild(0).name);
        if (InputFieldCaret != null)
        {
            //Debug.Log("InputFieldCaret != null " + MyRoot._lastMailText.lastLetterTransform.position);
            //Debug.Log("InputFieldCaret.position1 = " + InputFieldCaret.position);
            /*InputFieldCaret*/
            MyInputCaret.transform.position = MyRoot._lastMailText.lastLetterTransform.position;
            //Debug.Log("InputFieldCaret.position2 = " + InputFieldCaret.position);
        }
    }
    void Start()
    {
#if UNITY_WSA
        StartCoroutine(CursorBlink());
#else 
        MyInputCaret.gameObject.SetActive(false);
#endif
    }
    int _blinkCounter = 0;
    IEnumerator CursorBlink()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.425f);
            Color cursorColor = MyInputCaret.color;
            cursorColor.a = _blinkCounter++ % 2 == 0 ? 0f : 1f;
            MyInputCaret.color = cursorColor;
        }
    }
    public void OnInputChange()
    {
        //Debug.Log("OnInputChange + " + typing + " (MyRoot._lastMailText == null)" + (MyRoot._lastMailText == null));
        //if (!typing) return;
        if (MyRoot._lastMailText == null) return;
        
        MyRoot._lastMailText.MailText.text = Input.text;

        if (Input.text.Length > 0)
        {
            char lastChar = Input.text[Input.text.Length - 1];
            if (lastChar == ' ' || lastChar == ',' || lastChar == ';')
            {
                Input.text = Input.text.Substring(0, Input.text.Length - 1);
                OnInputEnd(true);
                return;
            }
        }
        
        MyRoot._lastMailText = null;//force recalculate

        //Debug.Log("OnInputChange MyRoot.RecalculatePositions();");
        MyRoot.RecalculatePositions();

        StartCoroutine(Main.Instance.RefreshAfterOneFrame(MyRoot._lastMailText.lastLetterTransform.gameObject));
    }
    public void OnInputEnd()
    {
        OnInputEnd(false);
    }
    public void OnInputEnd(bool continueTyping)
    {
        //Debug.Log("OnInputEnd + typing = " + typing);
        if (!typing) return;
        typing = continueTyping;
        _mailText = Input.text;

        bool mailOK = true;
        if (string.IsNullOrEmpty(_mailText) || !Util.IsEmail(_mailText))
        {
            MyRoot.RemoveLastMailText();
            if (!string.IsNullOrEmpty(_mailText)) Main.Instance.Notify(Settings.EMAIL_NOT_VALID);
            //return;
            mailOK = false;
        } else if (MyRoot.Mails.Contains(_mailText))
        {
            MyRoot.RemoveLastMailText();
            if (!string.IsNullOrEmpty(_mailText)) Main.Instance.Notify(Settings.EMAIL_ALREADY_ADDED);
            //return;
            mailOK = false;
        }
        else
        {
            MyRoot._lastMailText.Created = true;
        }

        if(mailOK) MyRoot.AddMail(_mailText);

        if (_mailText.Length > 50)
        {
            _mailText = _mailText.Substring(0, 50) + "...";
        }
        //Input.text = string.Empty;
        if (MyRoot._lastMailText != null)
            StartCoroutine(Main.Instance.RefreshAfterOneFrame(MyRoot._lastMailText.gameObject));

        MyRoot._lastMailText = null;//force recalculate
        MyRoot.RecalculatePositions();

        if(continueTyping) StartCoroutine(AddMailNotCreated());
        else
        {
            MailPopup _mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
            if (_mailPopup != null)
                _mailPopup.SubjectInputField.Select();
        }
        
    }
}
