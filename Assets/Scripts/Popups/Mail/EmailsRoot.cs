﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class EmailsRoot : MonoBehaviour
{
    public MailInput _mailInput;
    List<MailItem> _mailItems = new List<MailItem>();

    public List<string> Mails { get; } = new List<string>();
    public void AddMail(string mailText)
    {
        if (string.IsNullOrEmpty(mailText)) return;
        if(!Mails.Contains(mailText))
            Mails.Add(mailText);

        MailPopup _mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
        if (_mailPopup != null)
            _mailPopup.CheckMail();
    }

    #region orientation
    RectTransform _rect;
    LayoutElement _layoutElement;

    float _rectWidth = 0f;
    private void Awake()
    {
        _rect = GetComponent<RectTransform>();
        _layoutElement = GetComponent<LayoutElement>();
    }
    private void Update()
    {
        //Debug.Log("EmailsRoot>Update" + Mails.Count);
    }
    private IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        _rectWidth = _rect.rect.width;
        OrientationHandler.Instance.SubscribeOrientationChanged(OrientationChanged);
    }
    void OrientationChanged(Orientation newOrientation)
    {
        float newRectWidth = _rect.rect.width;
        if (_rectWidth != newRectWidth)
        {
            _rectWidth = newRectWidth;
            Debug.Log("OrientationChanged MyRoot.RecalculatePositions();");
            RecalculatePositions();
        }
    }
    #endregion
    
    const float TOPPADDING = 18f;
    const float BETWEENMAILSPADDING = 18f;
    const float MAILHEIGHT = 108f;
    int _currentLinesNo = 0;
    [HideInInspector] public MailItem _lastMailText = null;
    public void RecalculatePositions()
    {
        _currentLinesNo = 0;
        foreach (MailItem mailItem in _mailItems)
            PositionateMail(mailItem);

        float newHeight = (_currentLinesNo + 1) * (MAILHEIGHT + TOPPADDING) + TOPPADDING;

        MailPopup _mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
        if (_mailPopup != null)
            _mailPopup.ChangeVoidHeight(_layoutElement.preferredHeight, newHeight);

        _layoutElement.preferredHeight = newHeight;
        //Debug.Log("_layoutElement.preferredHeight = " + _layoutElement.preferredHeight + ", _currentLinesNo = " + _currentLinesNo);
    }
    void PositionateMail(MailItem mailItem)
    {
        float mostRightPoint = 0f;
        if (_lastMailText != null) mostRightPoint = _lastMailText.MostRightPoint + BETWEENMAILSPADDING;

        if (mostRightPoint + mailItem.Width > _rectWidth)
        {
            _currentLinesNo++;
            mostRightPoint = 0f;
        }
        
        mailItem.Position = new Vector2(mostRightPoint, -(_currentLinesNo * (MAILHEIGHT + TOPPADDING) + TOPPADDING));

        _lastMailText = mailItem;
        _lastMailText.Rect.ForceUpdateRectTransforms();
    }

    void AddEmail(MailItem mailItem)
    {
        PositionateMail(mailItem);
        _mailItems.Add(mailItem);
    }

    public IEnumerator AddEmail(GameObject MailItemPrefab, bool created)
    {
        MailItem mailItem = Instantiate(MailItemPrefab, transform).GetComponent<MailItem>();
        mailItem.Root = this;
        mailItem.Created = created;
        mailItem.Position = Vector2.one * 1000f;
        if (!created) mailItem.MailText.text = string.Empty;

        //mailItem.MailText.text = "kdje@spinview.io";
        mailItem.Rect.ForceUpdateRectTransforms();

        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        AddEmail(mailItem);

        //Transform InputFieldCaret = transform.GetChild(0).GetChild(0);
        if (_mailInput.MyInputCaret != null)
        {
            //Debug.Log("AddEmail  InputFieldCaret.position = " + InputFieldCaret.position);
            _mailInput.MyInputCaret.transform.position = _lastMailText.lastLetterTransform.position;
        }
    }

    public void RemoveLastMailText()
    {
        if (_mailItems.Count == 0) return;
        MailItem mailItem = _mailItems[_mailItems.Count - 1];
        _mailItems.RemoveAt(_mailItems.Count - 1);

        Destroy(mailItem.gameObject);
    }
    public void DeleteMail(string mail)
    {
        MailItem mailForDestroying = null;
        foreach (MailItem item in _mailItems)
            if (item.MailText.text == mail)
            {
                mailForDestroying = item;
                break;
            }
        if(mailForDestroying != null)
        {
            _mailItems.Remove(mailForDestroying);
            Destroy(mailForDestroying.gameObject);
        }
        Mails.Remove(mail);

        MailPopup _mailPopup = (MailPopup)PopupManager.Instance.GetPopup(PopupType.MAIL);
        if (_mailPopup != null)
            _mailPopup.CheckMail();
        
        _lastMailText = null;//force recalculate
        Debug.Log("DeleteMail RecalculatePositions();");
        RecalculatePositions();
    }
    private void OnDestroy()
    {
        OrientationHandler.Instance.UnsubscribeOrientationChanged(OrientationChanged);
    }
}
