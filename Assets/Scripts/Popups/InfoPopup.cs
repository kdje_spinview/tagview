﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPopup : Popup
{
    public Text text;

    public override void ClosePopup()
    {
        Destroy(this);
    }

    public override void OpenPopup(Dictionary<string, object> data)
    {
        if (data.ContainsKey("message"))
        {
            text.text = data["message"].ToString();
        }
    }
}
