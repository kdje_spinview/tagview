﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UpdatePopup : Popup
{
    public Button UpdateButton;


    private void Awake()
    {
        UpdateButton.onClick.AddListener(() => { AppstoreTestScene.OnUpdateButtonClick(); PopupManager.Instance.ClosePopup(); });
    }

    public override void ClosePopup()
    {
    }
    
    public override void OpenPopup(Dictionary<string, object> data)
    {
    }
}
