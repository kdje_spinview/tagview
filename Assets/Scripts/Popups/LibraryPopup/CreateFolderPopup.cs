﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CreateFolderPopup : Popup
{
    public Button CreateButton;
    public InputField FolderNameInputField;
    public GameObject ErrorMessage;

    public Sprite UISprite;
    public Sprite ErrorSprite;

    public Transform Root;

    private void Awake()
    {
        CreateButton.onClick.AddListener(Create);
        FolderNameInputField.onValueChanged.AddListener(FolderInputChanged);

        if (Util.GetDevice() == DeviceType.TABLET)
            Root.localScale = Vector3.one * 0.5f;
    }
    void Create()
    {
        bool error = false;
        switch (LibraryPopup.Instance.Mode)
        {
            case LibraryMode.LOCAL:
                if (LibraryPopup.Instance.FolderExistsLocal(FolderNameInputField.text))
                    error = true;
                break;
            case LibraryMode.PLATFORM:
                if (LibraryPopup.Instance.FolderExistsPlatform(FolderNameInputField.text))
                    error = true;
                break;
        }

        if (error)
        {
            ErrorMessage.SetActive(true);
            FolderNameInputField.image.sprite = ErrorSprite;
            return;
        }
        LibraryPopup.Instance.CreateNewFolder(FolderNameInputField.text);
        PopupManager.Instance.ClosePopup();//close this popup
    }
    void FolderInputChanged(string folderInput)
    {
        FolderNameInputField.image.sprite = UISprite;
        CreateButton.interactable = !string.IsNullOrEmpty(folderInput);
        ErrorMessage.SetActive(false);
    }
    public override void OpenPopup(Dictionary<string, object> data)
    {
        FolderNameInputField.text = string.Empty;
    }
    public override void ClosePopup()
    {
    }
}
