using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !UNITY_WSA
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
#endif
using System;
using Lean.Touch;

public class InteractionController : MonoBehaviour
{
    #region SINGLETON
    private static InteractionController instance = null;

    public static InteractionController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<InteractionController>();
            }

            return instance;
        }
    }
    #endregion

#if !UNITY_WSA
    ARPlaneManager planeManager;
    ARPointCloudManager pointManager;
    ARSessionOrigin sessionManager;
    public new Camera camera;

    public void SetupAR(GameObject arSession)
    {
        planeManager = arSession.GetComponent<ARPlaneManager>();
        pointManager = arSession.GetComponent<ARPointCloudManager>();
        sessionManager = arSession.GetComponent<ARSessionOrigin>();
        camera = arSession.transform.GetChild(0).gameObject.GetComponent<Camera>();
    }

#endif
}