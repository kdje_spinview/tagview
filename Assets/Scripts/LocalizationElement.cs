﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class LocalizationElement : MonoBehaviour
{
    public string Key;
    public string Before;
    public string After;

    Text _text;
    Text Text
    {
        get
        {
            if (_text == null) _text = GetComponent<Text>();
            return _text;
        }
    }

    private void OnEnable()
    {
        SetText();
    }

    public void SetText()
    {
        string newText = LocalizationController.Instance.GetTranslation(Key);
        if (!string.IsNullOrEmpty(newText))
            Text.text = Before + newText + After;
    }
}
