﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Newtonsoft.Json;


public enum Language
{
    ENGLISH = 0,//default
    GERMAN
}
public class LocalizationController : Singleton<LocalizationController>
{

    Dictionary<Language, Dictionary<string, string>> _loadedDictionary;
    Dictionary<Language, Dictionary<string, string>> LoadedDictionary
    {
        get
        {
            if (_loadedDictionary == null) _loadedDictionary = new Dictionary<Language, Dictionary<string, string>>();
            return _loadedDictionary;
        }
    }

    Dictionary<string, string> _currentDict;

    Language _currentLang = Language.ENGLISH;
    public Language CurrentLanguage
    {
        get
        {
            return _currentLang;
        }
        set
        {
            //if (_currentLang != value)
            {
                _currentLang = value;
                Prefs.Language = _currentLang;

                SetLanguage();
            }
        }
    }

    void SetLanguage()
    {
        LoadLanguage();

        LocalizationElement[] localizationElements = FindObjectsOfType<LocalizationElement>();
        foreach (LocalizationElement lE in localizationElements)
            lE.SetText();
    }
    void LoadLanguage()
    {
        if (LoadedDictionary.ContainsKey(_currentLang))
        {
            _currentDict = LoadedDictionary[_currentLang];
            return;
        }
        TextAsset textAsset = Resources.Load<TextAsset>("Languages/" + _currentLang.ToString());
        try
        {
            _currentDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(textAsset.ToString());
            LoadedDictionary.Add(_currentLang, _currentDict);
        }
        catch
        {
            return;
        }
    }
    public string GetTranslation(string key)
    {
        if(_currentDict != null && _currentDict.ContainsKey(key))
            return _currentDict[key];
        return key;
    }
}
