﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public enum SelectableType
{
    NONE,
    USERNAME, 
    PASSWORD,
    FORGOTTENPASSWORD
}
public class Selectable : MonoBehaviour, ISelectHandler//, IDeselectHandler// required interface when using the OnSelect method.
{
    public SelectableType type;

    public GameObject SelectedGraphics;
    
    //Do this when the selectable UI object is selected.
    public void OnSelect(BaseEventData eventData)
    {
        Debug.Log(this.gameObject.name + " was selected");
        LogIn.instance.SetSelectGraphics(type);
        //SelectedGraphics.SetActive(true);
    }

    /*public void OnDeselect(BaseEventData eventData)
    {
        Debug.Log(this.gameObject.name + " was deselected");
        SelectedGraphics.SetActive(false);
        MyPairSelectableGraphics.SetActive(false);
    }*/
}
