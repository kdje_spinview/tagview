﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class Settings {

    public const string URL = "http://arcontent.spinview.io/";
    public const string PLATFORM_URL = "https://api.spinviewglobal.com/api/v1/sites?details=folder&sort-by=site.name&sort-direction=asc&folder-id=624&_token=";
    public static string MAIN_URL = "";
    public static string JSON_URL = "";
    public static string JSON_LOCAL_DATA_PATH = Application.persistentDataPath + "/jsondata.txt";

    public static Dictionary<string, ModelData> PlatformModels = new Dictionary<string, ModelData>();
    public static Dictionary<string, ModelData> StoredModels = new Dictionary<string, ModelData>();

    public static LibraryData _library = new LibraryData();

    public static readonly string folderContentType = "3d_models";
    public static readonly string screenshotsFolder = "screenshots";

    public static readonly string defaultCompanyName = "SpinView";
    public static string companyName = "SpinView";
    public static string displayName = "Spin View";
    public static string email = "spinview@spinview.io";
    public static string accessToken = "";
    public static string companyID = "";
    public static string imageKey = "";
    public static string companyImageKey = "";
    public static string rootFolderID = "";
    public static string arSSFolderID = "";

    public static string platformScreenshotsFolder = "AR Screenshots";

    public static readonly string windowsSceneName = "Vuforia";
    public static readonly string arScene = "Main";

    #region MESSAGES
    public static readonly string EMAIL_SENT = "email_sent";
    public static readonly string EMAIL_NOT_VALID = "email_not_valid";
    public static readonly string EMAIL_ALREADY_ADDED = "email_already_added";
    public static readonly string NO_INTERNET = "no_internet_connection";
    public static readonly string SOMETHING_WENT_WRONG = "something_went_wrong";
    public static readonly string SELECT_PRODUCT_FIRST = "please_first_select_product";
    public static readonly string MODELS_UNAVAILABLE = "models_unavailable";
    public static readonly string NO_MODELS_AVAILABLE = "no_models_available";
    public static readonly string IMAGE_SAVED_LIBRARY = "images_saved_in_library";
    public static readonly string IMAGE_SAVED_DEVICE = "images_saved_on_device";
    public static readonly string INVALID_LOGIN = "invalid_username_or_password";
    public static readonly string SUBJECT_EMPTY = "subject_empty";
    public static readonly string LOADING = "loading";
    public static readonly string SCAN_SURFACE = "scan_surface";
    public static readonly string TAP_TO_PLACE = "tap_to_place";
    #endregion

    public enum ModelType
    {
        Image = 0,
        Model = 1
    }


    public static ModelType GetModelType(string type)
    {
        switch (type)
        {
            case "image":
                return ModelType.Image;
            case "3d_model":
                return ModelType.Model;
            default:
                return ModelType.Model;
        }
    }

    public static string GetBundleUrl(string name)
    {
        string s = MAIN_URL;
#if UNITY_ANDROID
        s += name.ToLower() + "_android.unity3d";
#elif UNITY_IOS
        s += name.ToLower() + "_ios.unity3d";
#else
        s += name.ToLower() + "_windows.unity3d";
#endif
        return s;
    }

    public static string GetPOSUrl(string id)
    {
        return "http://api.spinviewglobal.com/api/v1/sites/1/assets/" + id + "/prepare-download";
        //return "http://api.spinviewglobal.com/api/v1/sites/2576/assets/" + id + "?_token=" + Settings.accessToken;
    }

    public static string GetPrepareDownloadLink(string id)
    {
        return "http://api.spinviewglobal.com/api/v1/sites/1/assets/" + id + "/prepare-download?_token=" + Settings.accessToken;
    }

    public static string GetDownloadLink(string id)
    {
        return "http://api.spinviewglobal.com/api/v1/downloads/" + id;
    }

    public static string GetPOSTexture(string name)
    {
        return MAIN_URL + name.ToLower() + ".png";
    }

    public static string GetPOSMTL(string name)
    {
        return MAIN_URL + name.ToLower() + ".mtl";
    }

    public static string GetIconUrl(string name)
    {
        return MAIN_URL + name.ToLower() + "_icon.png";
    }

    public static string GetDeviceCountry()
    {
        //todo
        //write this method so it really checks where the device is located
        //string IP = IPManager.GetLocalIPAddress();
        return "Germany";
    }
}

public static class IPManager
{
#if !UNITY_WSA && !UNITY_EDITOR
    public static string GetLocalIPAddress()
    {
        var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }

        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }
#endif
}

[Serializable]
public class POSData
{
    public string name;
    public int version;
    public float scale;
}

[Serializable]
public class TransformParameters
{
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;

    public TransformParameters(Vector3 position, Vector3 rotation, Vector3 scale)
    {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }
}


[Serializable]
public class ModelData
{
    public string modelName;
    public Settings.ModelType type;
    public string folderName;
    public string imageURL;
    public string id;
    public string lastUpdateDate;
    public Vector2 dimmensions;

    public ModelData()
    {
        this.modelName = "Default";
        this.type = Settings.ModelType.Model;
        this.folderName = "Root";
        this.imageURL = "";
        this.id = "";
        this.lastUpdateDate = "";
        this.dimmensions = new Vector2();
    }

    public ModelData(string modelName, Settings.ModelType type, string folderName, string imageURL, string id, string lastUpdateDate, Vector2 dimmensions)
    {
        this.modelName = modelName;
        this.type = type;
        this.folderName = folderName;
        this.imageURL = imageURL;
        this.id = id;
        this.lastUpdateDate = lastUpdateDate;
        this.dimmensions = dimmensions;
    }

}

