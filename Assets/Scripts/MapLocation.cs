﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_ANDROID
using UnityEngine.Android;
#endif

public class MapLocation : MonoBehaviour
{
    [HideInInspector] public byte[] mapBytes;
    private IEnumerator Download(string mapApi)
    {      
        Debug.Log("MapImage: Download");
               
        int token = WebRequest.GetNewToken();
        yield return WebRequest.Get(token, mapApi);
        if (!WebRequest.Error(token))
        {
            Debug.Log("MapImage: done");

            mapBytes = WebRequest.BytesData(token);
            WebRequest.RemoveData(token);
            mapApi = "";
        }
        else
        {
            Debug.Log("MapImage: ERROR");
        }
        Debug.Log("MapImage: end");
    }

    public void CreateMapImage(bool save = true)
    {
        StartCoroutine(GetGPS(save));
    }

    public IEnumerator GetGPS(bool save)
    {
        print("JOKAN GPS Start");
        // First, check if user has location service enabled

#if !UNITY_EDITOR
        if (!Input.location.isEnabledByUser)
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            Permission.RequestUserPermission(Permission.FineLocation);
#endif
            yield break;
        }
#endif

        // Start service before querying location
        Input.location.Start();
        print("JOKAN GPS Started");
        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("JOKAN GPS Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("JOKAN GPS Unable to determine device location");
            yield break;
        }
        else if(save)
        {
            string coords = Input.location.lastData.latitude.ToString("0.000000") + "," + Input.location.lastData.longitude.ToString("0.000000");
            //Access granted and location value could be retrieved
            //print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
            string mapApi = "https://maps.googleapis.com/maps/api/staticmap?center=" + coords + "&scale=2&size=800x800&maptype=roadmap&markers=color:red%7C%7C" + coords + "&key=AIzaSyDWrZ4hD7m_OBC1S9S2LcyVm1-RGz7A-TI";
            Debug.Log("JOKAN GPS api = " + mapApi);
            StartCoroutine(Download(mapApi));
        }

        // Stop service if there is no need to query location updates continuously
        Input.location.Stop();
    }

}
