﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using Lean.Touch;

public enum AppMode
{
    IDLE,
    MODEL
}

public class GuiController : MonoBehaviour
{
    public Transform CaptureButtonPortrait;
    public Transform CaptureButtonPortraitRoot;
    
    public Transform SettingsButton;
    public GameObject SettingsUpdateNotification;
    
    public Button CaptureButton;
    

    private void Awake()
    {
        CaptureButton.onClick.AddListener(Main.Instance.ScreenShotButton);
        
        FixButtonsSize();
    }
    void FixButtonsSize()
    {
        DeviceType deviceType = Util.GetDevice();
        //portrait orientation//Orientation orientation = Util.GetOrientation();

        float _smaller = Util.ScreenSmallerSize;
        float _bigger = Util.ScreenBiggerSize;
        switch (deviceType)
        {
            case DeviceType.MOBILE:
                if (1080f < _smaller)
                {
                    CaptureButtonPortrait.transform.localScale = Vector3.one * 1080f / _smaller;
                    CaptureButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition *= 1080f / _smaller;
                }
                break;
            case DeviceType.TABLET:
                CaptureButtonPortrait.transform.localScale = Vector3.one / 2;
                CaptureButtonPortraitRoot.GetComponent<RectTransform>().anchoredPosition /= 2;
                break;
        }
    }
}
